/**
 * @file
 * Adds the entity-dialog path and class to matched links.
 */

(function($) {

Drupal.behaviors.entity_dialog = {
  attach: function (context, settings) {
    // Step through each link on the page that hasn't been rewritten.
    $('body').once('entity-dialog-setup', function() {
      // Want to deal with ones that do have href attributes.
      if (typeof($(this).attr('href')) !== 'undefined') {
        checkAndAttach($(this), Drupal.settings.entityDialog);
      }
    });

    // @todo This can be .on() if we have a dependency on jQuery update module.
    // Click event on the dialog close button.
    $(document).delegate('#entity-dialog .dialog-close', 'click', function(e) {
      e.preventDefault();
      $('#entity-dialog')
        .fadeOut('fast', function() {
          $(this).remove();
        });
    });
  }
};

/**
 * Decide whether to have entity_dialog intercept the link click, for the a tag
 * that is passed to us.
 *
 * @param e
 *   The <a> element we are checking and possibly intercepting.
 */
function checkAndAttach(e, crudPaths) {
  $.each(crudPaths, function(i, crudPath) {
    dialogHref = getDialogHref(e, crudPath);

    // If match then add class and bodgy the path.
    if (dialogHref) {
      e.attr('href', dialogHref).addClass('use-ajax entity-dialog');

      // Since we just added .use-ajax to an element, we now need to attach behaviours to it.
      Drupal.attachBehaviors(e);
    }
  });
}

/**
 * Get the dialog href. Return a modified href if the passed href matched the
 * passed CRUD path. Otherwise returns false.
 *
 * Links could take any of the following formats:
 *
 *     %basepath%crud-path (basePath is something either just / or /directory/ )
 *     %basepath%crud-path/
 *     %basepath%crud-path?query-string
 *
 * We will convert the following to something like:
 *
 *     %basepath%entity-dialog/crud-pat
 *     %basepath%entity-dialog/crud-path/
 *     %basepath%entity-dialog/crud-path?query-string (eg ?destination=blah/boo).
 */
function getDialogHref(e, crudPath) {
  var href = e.attr('href'),
      basePath = Drupal.settings.basePath,
      basePathFound = (href.indexOf(basePath) === 0);

  // Don't continue if the href doesn't start with the basepath. ie it must
  // begin with either a single / or a basepath which looks like /directory/
  if (!basePathFound) {
    return false;
  }

  // Strip the basepath off the href:
  // If the basepath is only a / then just remove the first character from the
  // href
  var basePathLength = basePath.length,
      noBasePath     = href.substring(basePathLength),
      strippedHref   = noBasePath.split('?')[0],
      hrefBits       = strippedHref.split('/'),
      crudBits       = crudPath.split('/'),
      matchFound     = pathMatch(hrefBits, crudBits);

  if (!matchFound) {
    return false;
  }

  // Determine the queryString, if any.
  var queryStringVal = noBasePath.split('?')[1],
      queryString = queryStringVal ? '?' + queryStringVal : '';

  // We have determined that the href was found to match the given CRUD path,
  // so this is an href we want to intercept.
  return basePath + 'entity-dialog/' + strippedHref + queryString;
}

/**
 * Work out how many parts of the supplied URL match the CRUD url.
 */
function pathMatch(hrefBits, crudBits) {
  // No match if this CRUD path has different length of pieces than the URL.
  if (crudBits.length != hrefBits.length) {
    return false;
  }

  var result = true;
  $.each(crudBits, function(i, crudBit) {
    // If this crud arg is a %something% placeholder, we don't care what the corresponding hrefBit is.
    if (crudBit.substr(0, 1) == '%') {
      // Continue to the next item.
      return true;
    }

    if (hrefBits[i] !== crudBit) {
      result = false;
      // Break out of the $.each entirely.
      return false;
    }
  });

  // Everything matched!
  return result;
}

})(jQuery);
