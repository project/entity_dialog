/**
 * @file
 * Entity Dialog JavaScript.
 */

(function($){$().ready(function(){

  /**
   * Entity Dialog status message AJAX framework command.
   */
  Drupal.ajax.prototype.commands.edStatus = function(ajax, response, status) {
    if (status == 'success') {
      // Fade in the status message and then fade out after 5 seconds.
      $(response.selector)
        .animate({
          opacity: 1,
          top: 10
        }, 250, 'easeInOutQuad')
        .delay(5000)
        .animate({
          opacity: 0,
          top: 20
        }, 500, 'easeInOutQuad');
    }
  }

  /**
   * Reload page AJAX framework command.
   */
  Drupal.ajax.prototype.commands.edReloadPage = function(ajax, response, status) {
    // Hide the contents of the dialog and show the spinner.
    $('#entity-dialog')
      .find('.inner')
      .addClass('loading')
      .find('.dialog-content')
      .fadeOut('slow');

    // Reload the page.
    window.location.reload(true);
  }

});})(jQuery);
